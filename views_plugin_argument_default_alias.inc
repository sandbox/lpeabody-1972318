<?php

/**
 * @file
 * Contains the node from URL alias argument default plugin.
 */

/**
 * Default argument plugin to extract a node via node alias
 *
 * There may come a need to add options in the future for specifying which
 * path arguments should be checked and in which order.  As for right now
 * though, this simple approach appears to be effective.
 */
class views_plugin_argument_default_alias extends views_plugin_argument_default {

  /**
   * Function for returning the argument value.
   * 
   * @return bool|int
   *   Either false or the node id.
   */
  function get_argument() {
    $path = request_path();
    $query_removed = preg_replace('/[#?].*$/', '', $path);
    $arguments = explode('/', $query_removed);

    return $this->reviewPathArguments($arguments, count($arguments));
  }

  /**
   * Recursive helper function for analyzing the path alias.
   * 
   * @return bool|int
   *   Either false or the node id.
   */
  protected function reviewPathArguments($arguments, $length) {
    if ($length == 0) {
      return FALSE;
    }
    $slices = array_slice($arguments, 0, $length);
    $check_path = implode('/', $slices);
    $system_path = drupal_lookup_path('source', $check_path);
    if ($system_path == FALSE) {
      return $this->reviewPathArguments($arguments, $length - 1);
    }
    else {
      $system_args = explode('/', $system_path);
      if ($system_args[0] == 'node' && is_numeric($system_args[1])) {
        return $system_args[1];
      }
      return FALSE;
    }
  }
}
