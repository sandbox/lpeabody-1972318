# Content from Alias

Content from Alias is a Drupal 7 module which adds an additional default value option when utilizing contextual filters within a view. The option added is "Content ID from Alias".

# How it Works

It works by analyzing the request path from right-to-left.  If the selection is recognized as a valid alias, and that alias translates into a system path to a node, then the node ID is returned.  Otherwise, the right-most path argument is dropped and the path is tested again, until no path arguments remain.  In the event that all remaining path arguments have been dropped, *false* is returned.