<?php
/**
 * @file
 * Contains the plugins configuration for the contextual filter dropdown.
 */

/**
 * Implements hook_views_plugins().
 */
function content_from_alias_views_plugins() {
  return array(
    'module' => 'views',
    'argument default' => array(
      'alias' => array(
        'title' => 'Content ID from Alias',
        'handler' => 'views_plugin_argument_default_alias',
      ),
    ),
  );
}
